mod common;

pub use common::*;  // `pub` only to silence unused code warnings
use spamassassin_milter::*;

#[test]
fn spamc_connection_error() {
    let mut builder = Config::builder();
    // `spamc` always ‘works’ even if it cannot actually reach `spamd`!
    // `--no-safe-fallback` prevents this masking of connection errors.
    builder.spamc_args(vec![
        String::from("--no-safe-fallback"),
        format!("--port={}", SPAMD_PORT),
    ]);
    let config = builder.build();

    let miltertest = spawn_miltertest_runner(file!());

    run("inet:3333@localhost", config).expect("milter execution failed");

    let exit_code = miltertest.join().expect("panic in miltertest runner");
    assert!(exit_code.success(), "miltertest returned error exit code");
}

use clap::{App, Arg, ArgMatches, Error, ErrorKind, Result};
use spamassassin_milter::Config;
use std::{net::IpAddr, process};

const ARG_AUTH_UNTRUSTED: &str = "AUTH_UNTRUSTED";
const ARG_DRY_RUN: &str = "DRY_RUN";
const ARG_MAX_MESSAGE_SIZE: &str = "MAX_MESSAGE_SIZE";
const ARG_MILTER_DEBUG_LEVEL: &str = "MILTER_DEBUG_LEVEL";
const ARG_PRESERVE_BODY: &str = "PRESERVE_BODY";
const ARG_PRESERVE_HEADERS: &str = "PRESERVE_HEADERS";
const ARG_REJECT_SPAM: &str = "REJECT_SPAM";
const ARG_TRUSTED_NETWORKS: &str = "TRUSTED_NETWORKS";
const ARG_VERBOSE: &str = "VERBOSE";
const ARG_SOCKET: &str = "SOCKET";
const ARG_SPAMC_ARGS: &str = "SPAMC_ARGS";

fn main() {
    use spamassassin_milter::{MILTER_NAME, VERSION};

    let matches = App::new(MILTER_NAME)
        .version(VERSION)
        .arg(Arg::with_name(ARG_AUTH_UNTRUSTED)
            .short("a")
            .long("auth-untrusted")
            .help("Treat authenticated senders as untrusted"))
        .arg(Arg::with_name(ARG_DRY_RUN)
            .short("n")
            .long("dry-run")
            .help("Process messages without applying any changes"))
        .arg(Arg::with_name(ARG_MAX_MESSAGE_SIZE)
            .short("s")
            .long("max-message-size")
            .value_name("BYTES")
            .help("Maximum message size to process"))
        .arg(Arg::with_name(ARG_MILTER_DEBUG_LEVEL)
            .long("milter-debug-level")
            .value_name("LEVEL")
            .possible_values(&["0", "1", "2", "3", "4", "5", "6"])
            .help("Set the milter library debug level")
            .hidden(true)  // not documented for now
            .hide_possible_values(true))
        .arg(Arg::with_name(ARG_PRESERVE_BODY)
            .short("B")
            .long("preserve-body")
            .help("Suppress rewriting of message body"))
        .arg(Arg::with_name(ARG_PRESERVE_HEADERS)
            .short("H")
            .long("preserve-headers")
            .help("Suppress rewriting of Subject/From/To headers"))
        .arg(Arg::with_name(ARG_REJECT_SPAM)
            .short("r")
            .long("reject-spam")
            .conflicts_with_all(&[ARG_PRESERVE_BODY, ARG_PRESERVE_HEADERS])
            .help("Reject messages flagged as spam"))
        .arg(Arg::with_name(ARG_TRUSTED_NETWORKS)
            .short("t")
            .long("trusted-networks")
            .value_name("NETS")
            .use_delimiter(true)
            .help("Trust connections from these networks"))
        .arg(Arg::with_name(ARG_VERBOSE)
            .short("v")
            .long("verbose")
            .help("Enable verbose operation logging"))
        .arg(Arg::with_name(ARG_SOCKET)
            .required(true)
            .help("Listening socket of the milter"))
        .arg(Arg::with_name(ARG_SPAMC_ARGS)
            .last(true)
            .multiple(true)
            .help("Additional arguments to pass to spamc"))
        .get_matches();

    let socket = matches.value_of(ARG_SOCKET).unwrap();
    let config = match build_config(&matches) {
        Ok(config) => config,
        Err(e) => {
            e.exit();
        }
    };

    eprintln!("{} {} starting", MILTER_NAME, VERSION);

    match spamassassin_milter::run(socket, config) {
        Ok(_) => {
            eprintln!("{} {} shut down", MILTER_NAME, VERSION);
        }
        Err(e) => {
            eprintln!("{} {} terminated with error: {}", MILTER_NAME, VERSION, e);
            process::exit(1);
        }
    }
}

fn build_config(matches: &ArgMatches<'_>) -> Result<Config> {
    let mut config = Config::builder();

    if let Some(bytes) = matches.value_of(ARG_MAX_MESSAGE_SIZE) {
        match bytes.parse() {
            Ok(bytes) => {
                config.max_message_size(bytes);
            }
            Err(_) => {
                return Err(Error::with_description(
                    &format!("Invalid value for max message size: \"{}\"", bytes),
                    ErrorKind::InvalidValue,
                ));
            }
        }
    }

    if let Some(nets) = matches.values_of(ARG_TRUSTED_NETWORKS) {
        config.use_trusted_networks(true);

        for net in nets.filter(|n| !n.trim().is_empty()) {
            // Both `ipnet::IpNet` and `std::net::IpAddr` inputs are supported.
            match net.parse().or_else(|_| net.parse::<IpAddr>().map(From::from)) {
                Ok(net) => {
                    config.trusted_network(net);
                }
                Err(_) => {
                    return Err(Error::with_description(
                        &format!("Invalid value for trusted network address: \"{}\"", net),
                        ErrorKind::InvalidValue,
                    ));
                }
            }
        }
    }

    if matches.is_present(ARG_AUTH_UNTRUSTED) {
        config.auth_untrusted(true);
    }
    if matches.is_present(ARG_DRY_RUN) {
        config.dry_run(true);
    }
    if matches.is_present(ARG_PRESERVE_BODY) {
        config.preserve_body(true);
    }
    if matches.is_present(ARG_PRESERVE_HEADERS) {
        config.preserve_headers(true);
    }
    if matches.is_present(ARG_REJECT_SPAM) {
        config.reject_spam(true);
    }
    if matches.is_present(ARG_VERBOSE) {
        config.verbose(true);
    }

    if let Some(level) = matches.value_of(ARG_MILTER_DEBUG_LEVEL) {
        config.milter_debug_level(level.parse().unwrap());
    }

    if let Some(spamc_args) = matches.values_of(ARG_SPAMC_ARGS) {
        config.spamc_args(spamc_args);
    };

    Ok(config.build())
}

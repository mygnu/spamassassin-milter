use std::mem;

// Design note: Header handling requires ASCII-case-insensitive map keys. This
// complexity could have been pushed out to the map key type (a newtype
// implementing `Hash` and `Eq` while using an ordinary `HashMap`), but we’ve
// found it simpler to introduce these custom collections instead, and use plain
// strings in the application logic.

/// A vector map with ASCII-case-insensitive `AsRef<str>` keys.
#[derive(Clone, Debug, Default)]
pub struct StrVecMap<K, V> {
    entries: Vec<(K, V)>,
}

impl<K, V> StrVecMap<K, V>
where
    K: AsRef<str>,
{
    pub fn new() -> Self {
        Self { entries: Default::default() }
    }

    pub fn iter(&self) -> impl Iterator<Item = (&K, &V)> {
        self.entries.iter().map(|e| (&e.0, &e.1))
    }

    pub fn keys(&self) -> impl Iterator<Item = &K> {
        self.iter().map(|e| e.0)
    }

    pub fn contains_key<Q: AsRef<str>>(&self, key: Q) -> bool {
        self.iter().any(|e| e.0.as_ref().eq_ignore_ascii_case(key.as_ref()))
    }

    pub fn get<Q: AsRef<str>>(&self, key: Q) -> Option<&V> {
        self.iter().find(|e| e.0.as_ref().eq_ignore_ascii_case(key.as_ref())).map(|e| e.1)
    }

    pub fn insert(&mut self, key: K, value: V) -> Option<V> {
        match self.entries.iter_mut().find(|e| e.0.as_ref().eq_ignore_ascii_case(key.as_ref())) {
            Some(e) => Some(mem::replace(&mut e.1, value)),
            None => {
                self.entries.push((key, value));
                None
            }
        }
    }
}

// Ad-hoc implementation, only does `to_owned` on the lookup key if the key is
// not yet present.
impl<V> StrVecMap<String, V> {
    pub fn insert_if_absent<K>(&mut self, key: K, value: V) -> Option<V>
    where
        K: AsRef<str>,
    {
        if self.contains_key(key.as_ref()) {
            Some(value)
        } else {
            self.entries.push((key.as_ref().to_owned(), value));
            None
        }
    }
}

/// A vector set containing ASCII-case-insensitive `AsRef<str>` elements.
#[derive(Clone, Debug, Default)]
pub struct StrVecSet<E> {
    map: StrVecMap<E, ()>,
}

impl<E> StrVecSet<E>
where
    E: AsRef<str>,
{
    pub fn new() -> Self {
        Self { map: StrVecMap::new() }
    }

    pub fn iter(&self) -> impl Iterator<Item = &E> {
        self.map.keys()
    }

    pub fn contains<Q: AsRef<str>>(&self, key: Q) -> bool {
        self.map.contains_key(key)
    }

    pub fn insert(&mut self, key: E) -> bool {
        self.map.insert(key, ()).is_none()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn map_contains_key() {
        let mut map = StrVecMap::new();
        map.insert("KEY1", 1);

        assert!(map.contains_key("key1"));
        assert!(!map.contains_key("key2"));
    }

    #[test]
    fn map_get() {
        let mut map = StrVecMap::new();
        map.insert("KEY1", 1);

        assert_eq!(map.get("key1"), Some(&1));
        assert_eq!(map.get("key2"), None);
    }

    #[test]
    fn map_insert() {
        let mut map = StrVecMap::new();

        assert_eq!(map.insert("KEY1", 1), None);
        assert_eq!(map.insert("KEY2", 2), None);
        assert_eq!(map.insert("key1", 3), Some(1));

        let mut iter = map.iter();
        assert_eq!(iter.next(), Some((&"KEY1", &3)));
        assert_eq!(iter.next(), Some((&"KEY2", &2)));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn map_insert_if_absent() {
        let mut map = StrVecMap::new();

        assert_eq!(map.insert_if_absent("KEY1", 1), None);
        assert_eq!(map.insert_if_absent("KEY2", 2), None);
        assert_eq!(map.insert_if_absent("key1", 3), Some(3));

        let mut iter = map.iter();
        assert_eq!(iter.next(), Some((&String::from("KEY1"), &1)));
        assert_eq!(iter.next(), Some((&String::from("KEY2"), &2)));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn set_insert() {
        let mut set = StrVecSet::new();

        assert!(set.insert("KEY1"));
        assert!(set.insert("KEY2"));
        assert!(!set.insert("key1"));

        let mut iter = set.iter();
        assert_eq!(iter.next(), Some(&"KEY1"));
        assert_eq!(iter.next(), Some(&"KEY2"));
        assert_eq!(iter.next(), None);
    }
}

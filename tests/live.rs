mod common;

pub use common::*;  // `pub` only to silence unused code warnings
use spamassassin_milter::*;

/// Runs a ‘live’ test against a real SpamAssassin server instance. This test is
/// run on demand, as SpamAssassin will actually analyse the input, and do DNS
/// queries etc.
#[test]
#[ignore]  // use option `--include-ignored` to run
fn live() {
    // Without `spamc_args` set, `spamc` will try to connect to the default
    // `spamd` port 783 (see also `/etc/services`).
    let config = Default::default();

    let miltertest = spawn_miltertest_runner(file!());

    run("inet:3333@localhost", config).expect("milter execution failed");

    let exit_code = miltertest.join().expect("panic in miltertest runner");
    assert!(exit_code.success(), "miltertest returned error exit code");
}

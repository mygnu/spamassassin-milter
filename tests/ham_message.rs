mod common;

pub use common::*;  // `pub` only to silence unused code warnings
use spamassassin_milter::*;

#[test]
fn ham_message() {
    let mut builder = Config::builder();
    builder.spamc_args(vec![format!("--port={}", SPAMD_PORT)]);
    let config = builder.build();

    let server = spawn_mock_spamd_server(SPAMD_PORT, |ham| {
        Ok(ham
            .replacen(
                "X-Spam-Checker-Version: BogusChecker 1.0.0\r\n",
                "X-Spam-Checker-Version: MyChecker 1.0.0\r\n",
                1,
            )
            .replacen("X-Spam-Report: Bogus report\r\n", "", 1)
            .replacen(
                "\r\n\r\n",
                "\r\n\
                 X-Spam-Custom: Custom-Value\r\n\
                 \r\n",
                1,
            ))
    });
    let miltertest = spawn_miltertest_runner(file!());

    run("inet:3333@localhost", config).expect("milter execution failed");

    let exit_code = miltertest.join().expect("panic in miltertest runner");
    assert!(exit_code.success(), "miltertest returned error exit code");

    server.join().expect("panic in mock spamd server");
}

mod common;

pub use common::*;  // `pub` only to silence unused code warnings
use spamassassin_milter::*;

#[test]
fn reject_spam() {
    let mut builder = Config::builder();
    builder.reject_spam(true);
    builder.spamc_args(vec![format!("--port={}", SPAMD_PORT)]);
    let config = builder.build();

    let server = spawn_mock_spamd_server(SPAMD_PORT, |spam| {
        Err(spam.replacen("\r\n\r\n", "\r\nX-Spam-Flag: YES\r\n\r\n", 1))
    });
    let miltertest = spawn_miltertest_runner(file!());

    run("inet:3333@localhost", config).expect("milter execution failed");

    let exit_code = miltertest.join().expect("panic in miltertest runner");
    assert!(exit_code.success(), "miltertest returned error exit code");

    server.join().expect("panic in mock spamd server");
}

mod common;

pub use common::*;  // `pub` only to silence unused code warnings
use spamassassin_milter::*;

#[test]
fn spam_message() {
    let mut builder = Config::builder();
    builder.spamc_args(vec![format!("--port={}", SPAMD_PORT)]);
    let config = builder.build();

    let server = spawn_mock_spamd_server(SPAMD_PORT, |spam| {
        let mut spam = spam
            .replacen("Subject: Test message\r\n", "Subject: [SPAM] Test message\r\n", 1)
            .replacen(
                "X-Spam-Checker-Version: BogusChecker 1.0.0\r\n",
                "X-Spam-Checker-Version: MyChecker 1.0.0\r\n",
                1,
            )
            .replacen("X-Spam-Report: Bogus report\r\n", "", 1)
            .replacen(
                "\r\n\r\n",
                "\r\n\
                 X-Spam-Flag: YES\r\n\
                 X-Spam-Custom: Custom-Value\r\n\
                 Content-Type: multipart/mixed; ...\r\n\
                 \r\n",
                1,
            );

        // Replace the message body with the SpamAssassin ‘report’.
        spam.replace_range(
            (spam.find("\r\n\r\n").unwrap() + 4)..,
            "Spam detection software has identified ...",
        );

        Err(spam)
    });
    let miltertest = spawn_miltertest_runner(file!());

    run("inet:3333@localhost", config).expect("milter execution failed");

    let exit_code = miltertest.join().expect("panic in miltertest runner");
    assert!(exit_code.success(), "miltertest returned error exit code");

    server.join().expect("panic in mock spamd server");
}

-- An authenticated sender is accepted, the message is not processed.

conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.conninfo(conn, "client.gluet.ch", "123.123.123.123")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.helo(conn, "mail.gluet.ch")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

-- `{auth_authen}` holds the SASL login name, if any.
local err = mt.macro(conn, SMFIC_MAIL, "{auth_authen}", "from@gluet.ch")
assert(err == nil, err)

local err = mt.mailfrom(conn, "from@gluet.ch")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_ACCEPT)

local err = mt.disconnect(conn)
assert(err == nil, err)

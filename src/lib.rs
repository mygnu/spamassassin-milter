//! The SpamAssassin Milter application library.

#![doc(html_root_url = "https://docs.rs/spamassassin-milter/0.1.0")]
#![macro_use]

macro_rules! verbose {
    ($config:ident, $($arg:tt)*) => {
        if $config.verbose() {
            ::std::eprintln!($($arg)*);
        }
    };
    ($($arg:tt)*) => {
        if $crate::config::get().verbose() {
            ::std::eprintln!($($arg)*);
        }
    };
}

mod callbacks;
mod client;
mod collections;
mod config;
mod email;
mod error;

use crate::callbacks::*;
pub use crate::config::{Config, ConfigBuilder};
use milter::{Milter, Result};

/// The name of the SpamAssassin Milter application.
pub const MILTER_NAME: &str = "SpamAssassin Milter";

/// The current version string of SpamAssassin Milter.
pub const VERSION: &str = "0.1.0";

/// Starts SpamAssassin Milter listening on the given socket using the supplied
/// configuration.
///
/// This is a blocking call.
///
/// # Errors
///
/// If execution of the milter fails, an error variant of type `milter::Error`
/// is returned.
///
/// # Examples
///
/// ```no_run
/// use spamassassin_milter::Config;
/// use std::process;
///
/// let socket = "inet:3000@localhost";
/// let config = Config::builder().build();
///
/// if let Err(e) = spamassassin_milter::run(socket, config) {
///     eprintln!("failed to run spamassassin-milter: {}", e);
///     process::exit(1);
/// }
/// ```
pub fn run(socket: &str, config: Config) -> Result<()> {
    milter::set_debug_level(config.milter_debug_level());

    config::init(config);

    Milter::new(socket)
        .name(MILTER_NAME)
        .on_negotiate(negotiate_callback)
        .on_connect(connect_callback)
        .on_helo(helo_callback)
        .on_mail(mail_callback)
        .on_rcpt(rcpt_callback)
        .on_data(data_callback)
        .on_header(header_callback)
        .on_eoh(eoh_callback)
        .on_body(body_callback)
        .on_eom(eom_callback)
        .on_abort(abort_callback)
        .on_close(close_callback)
        .run()
}

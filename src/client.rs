use crate::{
    config::Config,
    email::{self, Email, HeaderMap, HeaderRewriter},
    error::{Error, Result},
};
use milter::{ActionContext, SetErrorReply, Status};
use std::{
    any::Any,
    io::Write,
    os::unix::process::ExitStatusExt,
    process::{Child, Command, Stdio},
};

pub trait Process {
    fn connect(&mut self) -> Result<()>;
    fn writer(&mut self) -> &mut dyn Write;
    fn finish(&mut self) -> Result<Vec<u8>>;
    fn as_any(&self) -> &dyn Any;  // for testing only
}

pub struct Spamc {
    spamc_args: &'static [String],
    spamc: Option<Child>,
}

impl Spamc {
    const SPAMC_PROGRAM: &'static str = "spamc";

    pub fn new(spamc_args: &'static [String]) -> Self {
        Self {
            spamc_args,
            spamc: None,
        }
    }
}

impl Process for Spamc {
    fn connect(&mut self) -> Result<()> {
        // `Command::spawn` always succeeds when `spamc` can be invoked, even if
        // logically the command is invalid, eg if it uses non-existing options.
        let child = Command::new(Spamc::SPAMC_PROGRAM)
            .args(self.spamc_args)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()?;

        self.spamc = Some(child);

        Ok(())
    }

    fn writer(&mut self) -> &mut dyn Write {
        let spamc = self.spamc.as_mut().expect("spamc process not started");

        spamc.stdin.as_mut().unwrap()
    }

    fn finish(&mut self) -> Result<Vec<u8>> {
        let spamc = self.spamc.take().expect("spamc process not started");

        let output = spamc.wait_with_output()?;

        if output.status.success() {
            Ok(output.stdout)
        } else {
            Err(match output.status.code() {
                Some(code) => Error::Io(format!("spamc exited with status code {}", code)),
                None => Error::Io(format!(
                    "spamc terminated by signal {}",
                    output.status.signal().unwrap()
                )),
            })
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

impl Drop for Spamc {
    fn drop(&mut self) {
        // Kill (and wait on) `spamc`: a child process will continue to run even
        // after its `Child` handle has gone out of scope.
        if let Some(spamc) = self.spamc.as_mut() {
            // The results are no longer of interest at this point.
            let _ = spamc.kill();
            let _ = spamc.wait();
        }
    }
}

pub struct Client {
    process: Box<dyn Process>,
    sender: String,
    recipients: Vec<String>,
    headers: HeaderMap,
    bytes: usize,
}

impl Client {
    pub fn new(process: impl Process + 'static, sender: String) -> Self {
        Self {
            process: Box::new(process),
            sender,
            recipients: vec![],
            headers: HeaderMap::new(),
            bytes: 0,
        }
    }

    pub fn add_recipient(&mut self, recipient: String) {
        self.recipients.push(recipient);
    }

    pub fn bytes_written(&self) -> usize {
        self.bytes
    }

    pub fn connect(&mut self) -> Result<()> {
        self.process.connect()
    }

    // Implementation note: The send operations all may fail with an I/O error,
    // and so return a `Result<()>` that is then unceremoniously unwrapped with
    // `?` in the callback functions. This is acceptable, because we don’t
    // expect this to happen in normal circumstances, only when something is
    // wrong with `spamc` configuration or operation.

    pub fn send_envelope_sender(&mut self) -> Result<()> {
        let buf = format!("X-Envelope-From: {}\r\n", self.sender);

        self.bytes += self.process.writer().write(buf.as_bytes())?;

        Ok(())
    }

    pub fn send_envelope_recipients(&mut self) -> Result<()> {
        let buf = format!("X-Envelope-To: {}\r\n", self.recipients.join(",\r\n\t"));

        self.bytes += self.process.writer().write(buf.as_bytes())?;

        Ok(())
    }

    pub fn send_forged_received_header(
        &mut self,
        helo_host: &str,
        client_name_addr: &str,
        my_hostname: &str,
        mta: &str,
        tls: bool,
        queue_id: &str,
        date_time: &str,
    ) -> Result<()> {
        let buf = format!(
            "Received: from {} ({})\r\n\
             \tby {} ({}) with {} id {};\r\n\
             \t{}\r\n\
             \t(envelope-from {})\r\n",
            helo_host,
            client_name_addr,
            my_hostname,
            mta,
            if tls { "ESMTPS" } else { "ESMTP" },
            queue_id,
            date_time,
            self.sender
        );

        self.bytes += self.process.writer().write(buf.as_bytes())?;

        Ok(())
    }

    pub fn send_header(&mut self, name: &str, value: &str) -> Result<()> {
        // As requested during milter protocol negotiation, the value includes
        // leading whitespace. This lets us pass on whitespace exactly as is.
        let value = email::ensure_crlf(value);
        let buf = format!("{}:{}\r\n", name, value);

        if email::is_spam_assassin_header(name)
            || email::REWRITE_HEADERS.contains(name)
            || email::REPORT_HEADERS.contains(name)
        {
            self.headers.insert_if_absent(name, value);
        }

        self.bytes += self.process.writer().write(buf.as_bytes())?;

        Ok(())
    }

    pub fn send_eoh(&mut self) -> Result<()> {
        self.bytes += self.process.writer().write(b"\r\n")?;

        Ok(())
    }

    pub fn send_body_chunk(&mut self, bytes: &[u8]) -> Result<()> {
        self.bytes += self.process.writer().write(bytes)?;

        Ok(())
    }

    pub fn process(
        mut self,
        id: &str,
        actions: &(impl ActionContext + SetErrorReply),
        config: &Config,
    ) -> milter::Result<Status> {
        let output = match self.process.finish() {
            Ok(output) => output,
            Err(e) => {
                eprintln!("{}: failed to complete spamc communication: {}", id, e);
                return Ok(Status::Tempfail);
            }
        };

        let email = match Email::parse(&output) {
            Ok(email) => email,
            Err(e) => {
                eprintln!("{}: invalid response from spamc: {}", id, e);
                return Ok(Status::Tempfail);
            }
        };

        let mut rewriter = HeaderRewriter::new(self.headers, config);
        for header in email.header {
            rewriter.process_header(header.name, header.value);
        }

        let spam = rewriter.is_flagged_spam();

        if spam && config.reject_spam() {
            return reject_spam(id, actions, config);
        }

        rewriter.rewrite_spam_assassin_headers(id, actions)?;

        if spam {
            if !config.preserve_headers() {
                rewriter.rewrite_rewrite_headers(id, actions)?;
            }
            if !config.preserve_body() {
                rewriter.rewrite_report_headers(id, actions)?;
                email::replace_body(id, email.body, actions, config)?;
            }
        }

        Ok(Status::Accept)
    }
}

fn reject_spam(id: &str, actions: &impl SetErrorReply, config: &Config) -> milter::Result<Status> {
    Ok(if config.dry_run() {
        verbose!(config, "{}: rejected message flagged as spam [dry-run, not done]", id);
        Status::Accept
    } else {
        // These reply codes are the most appropriate according to RFCs 5321 and
        // 3463. The text is kept generic and makes no mention of SpamAssassin.
        actions.set_error_reply("550", Some("5.7.1"), vec!["Spam message refused"])?;

        verbose!(config, "{}: rejected message flagged as spam", id);
        Status::Reject
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{cell::RefCell, collections::HashSet};

    #[derive(Clone, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
    struct MockSpamc {
        buf: Vec<u8>,
        output: Option<Vec<u8>>,
    }

    impl MockSpamc {
        fn new() -> Self {
            Default::default()
        }

        fn with_output(output: Vec<u8>) -> Self {
            Self { output: Some(output), ..Default::default() }
        }
    }

    impl Process for MockSpamc {
        fn connect(&mut self) -> Result<()> {
            Ok(())
        }

        fn writer(&mut self) -> &mut dyn Write {
            &mut self.buf
        }

        fn finish(&mut self) -> Result<Vec<u8>> {
            Ok(if let Some(output) = &self.output {
                output.clone()
            } else {
                self.buf.clone()
            })
        }

        fn as_any(&self) -> &dyn Any {
            self
        }
    }

    fn as_mock_spamc(process: &dyn Process) -> &MockSpamc {
        process.as_any().downcast_ref().unwrap()
    }

    #[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
    enum Action {
        AddHeader(String, String),
        ReplaceHeader(String, usize, Option<String>),
        AppendBodyChunk(Vec<u8>),
        SetErrorReply(String, Option<String>, Vec<String>),
    }

    #[derive(Clone, Debug, Default)]
    struct MockActionContext {
        called: RefCell<Vec<Action>>,
    }

    impl MockActionContext {
        fn new() -> Self {
            Default::default()
        }
    }

    impl ActionContext for MockActionContext {
        fn add_header(&self, name: &str, value: &str) -> milter::Result<()> {
            Ok(self.called.borrow_mut().push(
                Action::AddHeader(name.to_owned(), value.to_owned())
            ))
        }

        fn replace_header(&self, name: &str, index: usize, value: Option<&str>) -> milter::Result<()> {
            Ok(self.called.borrow_mut().push(
                Action::ReplaceHeader(name.to_owned(), index, value.map(|v| v.to_owned()))
            ))
        }

        fn append_body_chunk(&self, bytes: &[u8]) -> milter::Result<()> {
            Ok(self.called.borrow_mut().push(
                Action::AppendBodyChunk(bytes.to_owned())
            ))
        }

        fn replace_sender(&self, _: &str, _: Option<&str>) -> milter::Result<()> {
            unimplemented!();
        }

        fn add_recipient(&self, _: &str, _: Option<&str>) -> milter::Result<()> {
            unimplemented!();
        }

        fn remove_recipient(&self, _: &str) -> milter::Result<()> {
            unimplemented!();
        }

        fn insert_header(&self, _: usize, _: &str, _: &str) -> milter::Result<()> {
            unimplemented!();
        }

        fn quarantine(&self, _: &str) -> milter::Result<()> {
            unimplemented!();
        }

        fn signal_progress(&self) -> milter::Result<()> {
            unimplemented!();
        }
    }

    impl SetErrorReply for MockActionContext {
        fn set_error_reply(
            &self,
            code: &str,
            ext_code: Option<&str>,
            msg_lines: Vec<&str>,
        ) -> milter::Result<()> {
            Ok(self.called.borrow_mut().push(Action::SetErrorReply(
                code.to_owned(),
                ext_code.map(|c| c.to_owned()),
                msg_lines.into_iter().map(|l| l.to_owned()).collect(),
            )))
        }
    }

    #[test]
    fn client_send_writes_bytes() {
        let spamc = MockSpamc::new();

        let mut client = Client::new(spamc, String::from("sender"));
        client.send_header("name1", " value1").unwrap();
        client.send_header("name2", " value2\n\tcontinued").unwrap();
        client.send_eoh().unwrap();
        client.send_body_chunk(b"body").unwrap();

        assert_eq!(client.bytes_written(), 48);
        assert_eq!(
            as_mock_spamc(client.process.as_ref()).buf,
            Vec::from(b"name1: value1\r\nname2: value2\r\n\tcontinued\r\n\r\nbody" as &[_])
        );
    }

    #[test]
    fn client_send_envelope_addresses() {
        let spamc = MockSpamc::new();

        let sender = "<sender@gluet.ch>";
        let recipient1 = "<recipient1@gluet.ch>";
        let recipient2 = "<recipient2@gluet.ch>";

        let mut client = Client::new(spamc, String::from(sender));
        client.add_recipient(String::from(recipient1));
        client.add_recipient(String::from(recipient2));

        client.send_envelope_sender().unwrap();
        client.send_envelope_recipients().unwrap();

        assert_eq!(
            as_mock_spamc(client.process.as_ref()).buf,
            Vec::from(format!(
                "X-Envelope-From: {}\r\n\
                 X-Envelope-To: {},\r\n\
                 \t{}\r\n",
                sender, recipient1, recipient2
            ).as_bytes())
        );
    }

    #[test]
    fn client_process_invalid_response() {
        let spamc = MockSpamc::with_output(b"invalid message response".to_vec());
        let actions = MockActionContext::new();
        let config = Default::default();

        let client = Client::new(spamc, String::from("sender"));
        let status = client.process("id", &actions, &config).unwrap();

        assert_eq!(status, Status::Tempfail);
    }

    #[test]
    fn client_process_reject_spam() {
        let spamc = MockSpamc::with_output(b"X-Spam-Flag: YES\r\n\r\n".to_vec());
        let actions = MockActionContext::new();
        let mut builder = Config::builder();
        builder.reject_spam(true);
        let config = builder.build();

        let client = Client::new(spamc, String::from("sender"));
        let status = client.process("id", &actions, &config).unwrap();

        assert_eq!(status, Status::Reject);

        let called = actions.called.borrow();
        assert_eq!(called.len(), 1);
        assert_eq!(
            called.first().unwrap(),
            &Action::SetErrorReply(
                "550".into(),
                Some("5.7.1".into()),
                vec!["Spam message refused".into()]
            )
        );
    }

    #[test]
    fn client_process_rewrite_spam() {
        let spamc = MockSpamc::with_output(
            b"X-Spam-Flag: YES\r\nX-Spam-Level: *****\r\n\r\nReport".to_vec(),
        );
        let actions = MockActionContext::new();
        let config = Default::default();

        let mut client = Client::new(spamc, String::from("sender"));
        client.send_header("x-spam-level", " *").unwrap();
        client.send_header("x-spam-report", " ...").unwrap();

        let status = client.process("id", &actions, &config).unwrap();

        assert_eq!(status, Status::Accept);

        let called = actions.called.borrow();
        assert_eq!(called.len(), 4);

        let mut expected = HashSet::new();
        expected.insert(Action::AddHeader("X-Spam-Flag".into(), " YES".into()));
        expected.insert(Action::ReplaceHeader("X-Spam-Level".into(), 1, Some(" *****".into())));
        expected.insert(Action::ReplaceHeader("X-Spam-Report".into(), 1, None));
        expected.insert(Action::AppendBodyChunk(b"Report".to_vec()));

        for a in called.iter() {
            assert!(expected.contains(&a));
        }
    }
}

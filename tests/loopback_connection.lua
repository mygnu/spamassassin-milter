-- 1) A connection from the loopback IP address is accepted.

conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.conninfo(conn, nil, "127.0.0.1")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_ACCEPT)

local err = mt.disconnect(conn)
assert(err == nil, err)

-- 2) A connection from an ‘unknown’ IP address (for example, from a UNIX
-- domain socket) is also accepted.

conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.conninfo(conn, nil, "unspec")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_ACCEPT)

local err = mt.disconnect(conn)
assert(err == nil, err)

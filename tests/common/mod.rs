use std::{
    ffi::OsString,
    io::{ErrorKind, Read, Write},
    net::{Ipv4Addr, Shutdown, SocketAddrV4, TcpListener},
    path::PathBuf,
    process::{Command, ExitStatus},
    thread::{self, JoinHandle},
    time::{Duration, Instant},
};

pub const SPAMD_PORT: u16 = 3783;  // mock port

pub type HamOrSpam = Result<String, String>;

/// Spawns a mock `spamd` server that echoes what it is sent after applying
/// transformation `f` to the message content and mock-classifying it as ham or
/// spam.
pub fn spawn_mock_spamd_server<F>(port: u16, f: F) -> JoinHandle<()>
where
    F: Fn(String) -> HamOrSpam + Send + 'static,
{
    let socket_addr = SocketAddrV4::new(Ipv4Addr::new(127, 0, 0, 1), port);
    let timeout = Duration::from_secs(15);

    thread::spawn(move || {
        let listener = TcpListener::bind(socket_addr).unwrap();
        listener.set_nonblocking(true).unwrap();

        let now = Instant::now();

        // This server expects and handles only a single connection, so that we
        // can `join` this thread in the tests and detect panics. A panic can be
        // triggered both in the handling code as well as due to the timeout.
        loop {
            match listener.accept() {
                Ok((mut stream, _)) => {
                    let mut buf = Vec::new();
                    stream.read_to_end(&mut buf).unwrap();
                    stream.write_all(process_message(buf, &f).as_bytes()).unwrap();
                    stream.shutdown(Shutdown::Write).unwrap();
                    break;
                }
                Err(e) if e.kind() == ErrorKind::WouldBlock => {
                    thread::sleep(Duration::from_millis(10));
                    if now.elapsed() > timeout {
                        panic!("mock spamd server timed out waiting for a connection");
                    }
                }
                Err(e) => {
                    panic!("mock spamd server could not open connection: {}", e);
                }
            }
        }
    })
}

// The SpamAssassin client/server protocol is here reverse-engineered in a very
// rudimentary fashion: Both client and server send a protocol header containing
// a content length indication and terminated with "\r\n\r\n". The payload is
// the email message itself with CRLF line endings.

const SPAMD_PROTOCOL_OK: &str = "SPAMD/1.1 0 EX_OK";

fn process_message<F>(buf: Vec<u8>, f: &F) -> String
where
    F: Fn(String) -> HamOrSpam,
{
    let mut msg = String::from_utf8(buf).unwrap();

    // Crude handling of the `spamc` client protocol: strip off everything
    // before and including the first "\r\n\r\n".
    let i = msg.find("\r\n\r\n").expect("spamc protocol header missing");
    msg.replace_range(..i + 4, "");

    match f(msg) {
        // Again very basic handling of the `spamd` server protocol: add a
        // forged protocol header terminated with "\r\n\r\n". (This is currently
        // not used in tests.)
        Ok(ham) => format!(
            "{}\r\nContent-length: {}\r\nSpam: False ; 4.0 / 5.0\r\n\r\n{}",
            SPAMD_PROTOCOL_OK,
            ham.len(),
            ham
        ),
        Err(spam) => format!(
            "{}\r\nContent-length: {}\r\nSpam: True ; 6.0 / 5.0\r\n\r\n{}",
            SPAMD_PROTOCOL_OK,
            spam.len(),
            spam
        ),
    }
}

const MILTERTEST_PROGRAM: &str = "miltertest";

pub fn spawn_miltertest_runner(test_file_name: &str) -> JoinHandle<ExitStatus> {
    // This thread is just for safety, in case the miltertest runner thread
    // below never manages to shut down the milter.
    let _timeout_thread = thread::spawn(|| {
        thread::sleep(Duration::from_secs(20));

        eprintln!("miltertest runner timed out");

        milter::shutdown();
    });

    let file_name = to_miltertest_file_name(test_file_name);

    thread::spawn(move || {
        // Wait just a little while to give the milter time to start up.
        thread::sleep(Duration::from_millis(100));

        let output = Command::new(MILTERTEST_PROGRAM)
            .arg("-s")
            .arg(&file_name)
            .output()
            .expect("miltertest execution failed");

        print_output_stream("STDOUT", output.stdout);
        print_output_stream("STDERR", output.stderr);

        milter::shutdown();

        output.status
    })
}

fn to_miltertest_file_name(file_name: &str) -> OsString {
    let mut path = PathBuf::from(file_name);
    path.set_extension("lua");
    path.into_os_string()
}

fn print_output_stream(name: &str, output: Vec<u8>) {
    if !output.is_empty() {
        let output = String::from_utf8(output).unwrap();

        eprintln!("{}:", name);

        if output.ends_with('\n') {
            eprint!("{}", &output)
        } else {
            eprintln!("{}", &output)
        }
    }
}

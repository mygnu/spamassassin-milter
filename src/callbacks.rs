use crate::{
    client::{Client, Spamc},
    config,
};
use chrono::Local;
use milter::{
    on_abort, on_body, on_close, on_connect, on_data, on_eoh, on_eom, on_header, on_helo, on_mail,
    on_negotiate, on_rcpt, Actions, Context, MacroValue, ProtocolOpts, Stage, Status,
};
use std::net::{IpAddr, Ipv4Addr, SocketAddr};

struct Connection {
    client_ip: IpAddr,
    helo_host: Option<String>,
    client: Option<Client>,
}

impl Connection {
    fn new(client_ip: IpAddr) -> Self {
        Self {
            client_ip,
            helo_host: None,
            client: None,
        }
    }
}

#[on_negotiate(negotiate_callback)]
fn handle_negotiate(
    ctx: Context<Connection>,
    actions: Actions,
    protocol_opts: ProtocolOpts,
) -> milter::Result<(Status, Actions, ProtocolOpts)> {
    let mut req_actions = Actions::empty();
    if !config::get().dry_run() {
        req_actions |= Actions::ADD_HEADER | Actions::REPLACE_HEADER;
        if !config::get().preserve_body() {
            req_actions |= Actions::REPLACE_BODY;
        }
    }

    let req_protocol_opts = ProtocolOpts::SKIP | ProtocolOpts::HEADER_LEADING_SPACE;

    assert!(actions.contains(req_actions), "required milter actions not supported");
    assert!(protocol_opts.contains(req_protocol_opts), "required milter protocol options not supported");

    ctx.api.request_macros(Stage::Connect, "")?;
    ctx.api.request_macros(Stage::Helo, "")?;
    ctx.api.request_macros(
        Stage::Mail,
        if config::get().auth_untrusted() { "" } else { "{auth_authen}" },
    )?;
    ctx.api.request_macros(Stage::Rcpt, "")?;
    ctx.api.request_macros(Stage::Data, "i j _ {tls_version} v")?;
    ctx.api.request_macros(Stage::Eoh, "")?;
    ctx.api.request_macros(Stage::Eom, "")?;

    Ok((Status::Continue, req_actions, req_protocol_opts))
}

#[on_connect(connect_callback)]
fn handle_connect(
    mut ctx: Context<Connection>,
    _: &str,
    socket_addr: Option<SocketAddr>,
) -> milter::Result<Status> {
    let ip = socket_addr.map_or_else(|| IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), |a| a.ip());

    if config::get().use_trusted_networks() {
        if config::get().is_in_trusted_networks(&ip) {
            verbose!("accepted connection from trusted network address {}", ip);
            return Ok(Status::Accept);
        }
    } else if ip.is_loopback() {
        verbose!("accepted local connection");
        return Ok(Status::Accept);
    }

    let conn = Connection::new(ip);
    ctx.data.replace(conn)?;

    Ok(Status::Continue)
}

#[on_helo(helo_callback)]
fn handle_helo(mut ctx: Context<Connection>, helo_host: &str) -> milter::Result<Status> {
    let conn = ctx.data.borrow_mut().unwrap();

    conn.helo_host = Some(helo_host.to_owned());

    Ok(Status::Continue)
}

#[on_mail(mail_callback)]
fn handle_mail(mut ctx: Context<Connection>, smtp_args: Vec<&str>) -> milter::Result<Status> {
    let conn = ctx.data.borrow_mut().unwrap();

    if !config::get().auth_untrusted() {
        if let Some(login) = ctx.api.macro_value("{auth_authen}")? {
            verbose!("accepted message from sender authenticated as \"{}\"", login);
            return Ok(Status::Accept);
        }
    }

    let spamc = Spamc::new(config::get().spamc_args());
    let sender = smtp_args[0].to_owned();
    conn.client = Some(Client::new(spamc, sender));

    Ok(Status::Continue)
}

#[on_rcpt(rcpt_callback)]
fn handle_rcpt(mut ctx: Context<Connection>, smtp_args: Vec<&str>) -> milter::Result<Status> {
    let conn = ctx.data.borrow_mut().unwrap();
    let client = conn.client.as_mut().unwrap();

    let recipient = smtp_args[0].to_owned();
    client.add_recipient(recipient);

    Ok(Status::Continue)
}

#[on_data(data_callback)]
fn handle_data(mut ctx: Context<Connection>) -> milter::Result<Status> {
    let conn = ctx.data.borrow_mut().unwrap();
    let client = conn.client.as_mut().unwrap();

    let id = queue_id(&ctx.api)?;

    if let Err(e) = client.connect() {
        eprintln!("{}: failed to start spamc: {}", id, e);
        return Ok(Status::Tempfail);
    }

    let client_ip = conn.client_ip.to_string();

    // Note that when SpamAssassin reports are enabled (`report_safe 1`), the
    // forged headers below are ‘leaked’ to users in the sense that they are
    // included inside the email MIME attachment in the new message body.

    client.send_envelope_sender()?;
    client.send_envelope_recipients()?;
    client.send_forged_received_header(
        conn.helo_host.as_ref().unwrap_or(&client_ip),
        ctx.api.macro_value("_")?.unwrap_or(&client_ip),
        ctx.api.macro_value("j")?.unwrap_or("localhost"),
        ctx.api.macro_value("v")?
            .and_then(|v| v.split_ascii_whitespace().next())
            .unwrap_or("Postfix"),
        ctx.api.macro_value("{tls_version}")?.is_some(),
        id,
        &Local::now().to_rfc2822(),
    )?;

    Ok(Status::Continue)
}

#[on_header(header_callback)]
fn handle_header(mut ctx: Context<Connection>, name: &str, value: &str) -> milter::Result<Status> {
    let conn = ctx.data.borrow_mut().unwrap();
    let client = conn.client.as_mut().unwrap();

    client.send_header(name, value)?;

    Ok(Status::Continue)
}

#[on_eoh(eoh_callback)]
fn handle_eoh(mut ctx: Context<Connection>) -> milter::Result<Status> {
    let conn = ctx.data.borrow_mut().unwrap();
    let client = conn.client.as_mut().unwrap();

    client.send_eoh()?;

    Ok(Status::Continue)
}

#[on_body(body_callback)]
fn handle_body(mut ctx: Context<Connection>, bytes: &[u8]) -> milter::Result<Status> {
    let conn = ctx.data.borrow_mut().unwrap();
    let client = conn.client.as_mut().unwrap();

    client.send_body_chunk(bytes)?;

    let max = config::get().max_message_size();
    Ok(if client.bytes_written() >= max {
        let id = queue_id(&ctx.api)?;
        verbose!("{}: skipping rest of message larger than {} bytes", id, max);
        Status::Skip
    } else {
        Status::Continue
    })
}

#[on_eom(eom_callback)]
fn handle_eom(mut ctx: Context<Connection>) -> milter::Result<Status> {
    let conn = ctx.data.borrow_mut().unwrap();
    let client = conn.client.take().unwrap();

    let id = queue_id(&ctx.api)?;
    let config = config::get();

    client.process(id, &ctx.api, config)
}

#[on_abort(abort_callback)]
fn handle_abort(mut ctx: Context<Connection>) -> milter::Result<Status> {
    ctx.data.borrow_mut().unwrap().client = None;

    Ok(Status::Continue)
}

#[on_close(close_callback)]
fn handle_close(mut ctx: Context<Connection>) -> milter::Result<Status> {
    ctx.data.take()?;

    Ok(Status::Continue)
}

fn queue_id(macros: &impl MacroValue) -> milter::Result<&str> {
    macros.macro_value("i").map(|i| i.unwrap_or("NONE"))
}

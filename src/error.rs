use std::{
    error,
    fmt::{self, Display, Formatter},
    io, result,
};

pub type Result<T> = result::Result<T, Error>;

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Error {
    ParseEmail,
    // For our purposes it is enough to record just the error message of I/O
    // errors, no need to keep the `io::Error` itself around.
    Io(String),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use Error::*;

        match self {
            ParseEmail => write!(f, "failed to parse email"),
            Io(msg) => msg.fmt(f),
        }
    }
}

impl error::Error for Error {}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Error::Io(format!("{}", error))  // just record the error message
    }
}

impl From<Error> for milter::Error {
    fn from(error: Error) -> Self {
        milter::Error::Custom(error.into())
    }
}

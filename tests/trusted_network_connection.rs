mod common;

pub use common::*;  // `pub` only to silence unused code warnings
use spamassassin_milter::*;

#[test]
fn trusted_network_connection() {
    let mut builder = Config::builder();
    builder.trusted_network("123.120.0.0/14".parse().unwrap());
    let config = builder.build();

    let miltertest = spawn_miltertest_runner(file!());

    run("inet:3333@localhost", config).expect("milter execution failed");

    let exit_code = miltertest.join().expect("panic in miltertest runner");
    assert!(exit_code.success(), "miltertest returned error exit code");
}

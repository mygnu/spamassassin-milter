-- When no `spamd` server is available, `spamc` fails to connect.

conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.conninfo(conn, "client.gluet.ch", "123.123.123.123")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.helo(conn, "mail.gluet.ch")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.mailfrom(conn, "from@gluet.ch")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.rcptto(conn, "to@gluet.ch")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

SMFIC_DATA = string.byte("T")  -- SMFIC_DATA not exported by miltertest
local err = mt.macro(conn, SMFIC_DATA,
    "i", "1234567ABC",
    "j", "localhost",
    "_", "client.gluet.ch [123.123.123.123]",
    "{tls_version}", "TLSv1.2",
    "v", "Postfix 3.3.0")
assert(err == nil, err)

-- When `spamc` cannot connect to `spamd`, it will retry several times. That is
-- why this test can proceed all the way to the `eom` stage where the failure
-- finally surfaces.

local err = mt.data(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.header(conn, "From", "from@gluet.ch")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)
local err = mt.header(conn, "To", "to@gluet.ch")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)
local err = mt.header(conn, "Subject", "Test message")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)
local err = mt.header(conn, "Message-ID", string.format("<%06d@gluet.ch>", math.random(999999)))
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)
local err = mt.header(conn, "Date", os.date("%a, %d %b %Y %H:%M:%S %Z"))
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.eoh(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.bodystring(conn, "Test message body")
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

local err = mt.eom(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_TEMPFAIL)

local err = mt.disconnect(conn)
assert(err == nil, err)

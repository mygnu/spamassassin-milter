mod common;

pub use common::*;  // `pub` only to silence unused code warnings
use spamassassin_milter::*;

#[test]
fn skip_oversized() {
    let mut builder = Config::builder();
    builder.max_message_size(512);
    builder.spamc_args(vec![format!("--port={}", SPAMD_PORT)]);
    let config = builder.build();

    let server = spawn_mock_spamd_server(SPAMD_PORT, Ok);
    let miltertest = spawn_miltertest_runner(file!());

    run("inet:3333@localhost", config).expect("milter execution failed");

    let exit_code = miltertest.join().expect("panic in miltertest runner");
    assert!(exit_code.success(), "miltertest returned error exit code");

    server.join().expect("panic in mock spamd server");
}
